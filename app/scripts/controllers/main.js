'use strict';

/**
 * @ngdoc function
 * @name ngYuichartsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ngYuichartsApp
 */
angular.module('ngYuichartsApp')
  .controller('MainCtrl', function ($scope) {
    var data = [
      {category: '2014', a: 10, b: 24, c: 0, d: 0},
      {category: '2015', a: 16, b: 22, c: 44, d: 0},
      {category: '2016', a: 28, b: 19, c: 0, d: 14},
      {category: '2017', a: 10, b: 24, c: 0, d: 0},
      {category: '2018', a: 16, b: 22, c: 44, d: 0},
      {category: '2019', a: 28, b: 19, c: 0, d: 14},
      {category: '2020', a: 10, b: 24, c: 0, d: 0},
      {category: '2021', a: 16, b: 22, c: 44, d: 0},
      {category: '2022', a: 28, b: 19, c: 0, d: 14},
      {category: '2023', a: 10, b: 24, c: 0, d: 0},
      {category: '2024', a: 16, b: 22, c: 44, d: 0},
      {category: '2025', a: 28, b: 19, c: 0, d: 14},
      {category: '2026', a: 10, b: 24, c: 0, d: 0},
      {category: '2027', a: 16, b: 22, c: 44, d: 0},
      {category: '2028', a: 28, b: 19, c: 0, d: 14}
    ];
    YUI().use('charts-legend', function (Y) {
      // Charts is available and ready for use. Add implementation
      // code here.
      var styleDef = {
        series: {
          a: {
            marker: {
              fill: {
                color: '#75C044'
              }
            },
            line: {
              color: '#ff0000'
            }
          },
          b: {
            marker: {
              fill: {
                color: '#007CBF'
              }
            },
            line: {
              color: '#999'
            }
          },
          c: {
            marker: {
              fill: {
                color: '#CDDC29'
              }
            },
            line: {
              color: '#777'
            }
          },
          d: {
            marker: {
              fill: {
                color: '#3C495D'
              }
            },
            line: {
              color: '#444'
            }
          }
        }
      };
      var mychart = new Y.Chart({
        legend: {
          position: "right",
          width: 300,
          height: 300,
          styles: {
            hAlign: "center",
            hSpacing: 4
          }
        },
        dataProvider: data,
        render: '#myChart',
        stacked: true,
        type: 'column',
        styles: styleDef
      });
    });
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
