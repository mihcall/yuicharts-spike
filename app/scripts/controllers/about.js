'use strict';

/**
 * @ngdoc function
 * @name ngYuichartsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ngYuichartsApp
 */
angular.module('ngYuichartsApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
